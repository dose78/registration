# Registration

This is a Spring boot project with auth) integration. This is the backend part of the registration app. You can find the front end part **[here](https://bitbucket.org/dose78/registraionui/src/master/)**
## Development server

Navigate to `http://localhost:8080/` after running the tomcat server

## Notes

Note that you need to createa database in order to run this app. I've used mysql as the database provider. A creation script is provided under the resources/sql folder
