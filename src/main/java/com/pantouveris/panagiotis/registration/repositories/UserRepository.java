package com.pantouveris.panagiotis.registration.repositories;

import com.pantouveris.panagiotis.registration.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
