package com.pantouveris.panagiotis.registration.controllers;

import com.pantouveris.panagiotis.registration.models.User;
import com.pantouveris.panagiotis.registration.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    // when a user hits tha base requestMapping url this method is called
    @GetMapping
    public List<User> users(){
        return userRepository.findAll();
    }

    //this method handles what form creates on submission
    @PostMapping //this means the method responds to post
    @ResponseStatus(HttpStatus.OK)
    public void create(@RequestBody User user){
        userRepository.save(user);
    }

    // this is used to retrieve a User by id
    @GetMapping("/{id}") // by adding an id to the base path we say which User we want to retrieve
    public User get(@PathVariable("id") long id){
        return userRepository.getOne(id);
    }
}
