create table user (
  id      int         not null
    primary key,
  name    varchar(45) null,
  surname varchar(45) null,
  phone   varchar(45) null,
  email   varchar(45) null,
  gender  varchar(45) null,
  birth   date        null,
  contact tinyint     null,
  age     int         null
);

create table hibernate_sequence (
  next_val bigint null
);

